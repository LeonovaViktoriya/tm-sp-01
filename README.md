# tm-sp-01

#### Software requirements
```
java 1.8.0_192, maven 3.3.9
```
#### Technology stack
```
jdk 1.8, maven 3
```
#### Developer

```
Leonova Viktoriya
e-mail: leonovaviktoria88@gmail.com
```

#### Maven commands

```
mvn clean install
```

#### Execution commands

```
 tm-sp-01-1.0-SNAPSHOT.war add in tomcat then install
 ```