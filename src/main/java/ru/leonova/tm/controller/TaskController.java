package ru.leonova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.service.ITaskService;

import java.text.ParseException;
import java.util.*;

@Controller
public class TaskController extends AbstractData{

    @Autowired
    private ITaskService taskService;

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public String allTasks(Model model) {
        List<Task> tasks = taskService.findAll();
        model.addAttribute("tasks", tasks);
        return "task/tasks";
    }

    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.GET)
    public String allTasksByProject(Model model, @PathVariable("id") String id) {
        List<Task> tasks = taskService.findAllByProjectId(id);
        model.addAttribute(id);
        model.addAttribute("tasks", tasks);
        return "task/tasks";
    }

    @RequestMapping(value = "/tasks/createForProject/{id}", method = RequestMethod.GET)
    public ModelAndView addPage2(@PathVariable String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id", id);
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
        modelAndView.addObject(statuses);
        modelAndView.addObject(new Task());
        modelAndView.setViewName("task/createForProject");
        return modelAndView;
    }

    @RequestMapping(value = "/tasks/add/{id}", method = RequestMethod.POST)
    public String addTaskForProject(@PathVariable("id") String id, @ModelAttribute("name") String name,
                                    @ModelAttribute("description") String desc, @ModelAttribute("dateStart") String dateStart,
                                    @ModelAttribute("dateEnd") String dateEnd, @ModelAttribute("status") Status status) throws ParseException {
        Task task = new Task();
        task.setTaskId(UUID.randomUUID().toString());
        task.setDescription(desc);
        task.setDateStart(dateFormat.parse(dateStart));
        task.setDateEnd(dateFormat.parse(dateEnd));
        task.setDateSystem(new Date());
        task.setName(name);
        task.setStatus(status);
        Project project = new Project();
        project.setProjectId(id);
        task.setProject(project);
        taskService.create(task);
        return "redirect:/tasks";
    }

//    @RequestMapping(value = "/tasks/add", method = RequestMethod.GET)
//    public ModelAndView addPage() {
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("task/editTask");
//        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
//        modelAndView.addObject(statuses);
//        return modelAndView;
//    }


    @RequestMapping(value = "/tasks/delete/{id}", method = RequestMethod.GET)
    public String deleteTasks(@PathVariable("id") String id) {
        Task task = taskService.findOneById(id);
        taskService.removeTask(task);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("task", task);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/tasks/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") String id) {
        Task task = taskService.findOneById(id);
        ModelAndView modelAndView = new ModelAndView();
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
        modelAndView.addObject(statuses);
        modelAndView.setViewName("task/editTask");
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @RequestMapping(value = "/tasks/edit/{id}", method = RequestMethod.POST)
    public String editTask(@PathVariable("id") String id, @ModelAttribute("name") String name, @ModelAttribute("description") String desc,
                           @ModelAttribute("dateStart") String dateStart,
                           @ModelAttribute("dateEnd") String dateEnd, @ModelAttribute("status") Status status) throws ParseException {
        Task task = taskService.findOneById(id);
        if (task != null) {
            task.setDescription(desc);
            task.setName(name);
            task.setDateSystem(new Date());
            task.setDateStart(dateFormat.parse(dateStart));
            task.setDateEnd(dateFormat.parse(dateEnd));
            task.setStatus(status);
            taskService.update(task);
        }
        return "redirect:/tasks";
    }
}
