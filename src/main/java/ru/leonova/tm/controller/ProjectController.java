package ru.leonova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.service.IProjectService;
import ru.leonova.tm.service.ITaskService;
import ru.leonova.tm.service.ProjectService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class ProjectController extends AbstractData{

    @Autowired
    private IProjectService projectService;
    @Autowired
    private ITaskService taskService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public String allProjects(Model model) {
        List<Project> projects = projectService.findAll();
        model.addAttribute("projects", projects);
        return "project/projects";
    }

    @RequestMapping(value = "/projects/delete/{id}", method = RequestMethod.GET)
    public String deleteProjects(@PathVariable("id") String id) {
        Project project = projectService.findOneById(id);
        taskService.removeAllTasksByProjectId(id);
        projectService.removeProject(project);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("project", project);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/projects/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") String id) {
        Project project = projectService.findOneById(id);
        ModelAndView modelAndView = new ModelAndView();
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
        modelAndView.addObject(statuses);
        modelAndView.setViewName("project/editProject");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @RequestMapping(value = "/projects/edit/{id}", method = RequestMethod.POST)
    public String editProjects(@PathVariable("id") String id, @ModelAttribute("name") String name, @ModelAttribute("description") String desc,
                               @ModelAttribute("dateStart") String dateStart, @ModelAttribute("dateEnd") String dateEnd, @ModelAttribute("status") Status status) throws ParseException {
        Project project = projectService.findOneById(id);
        project.setDescription(desc);
        project.setName(name);
        project.setDateStart(dateFormat.parse(dateStart));
        project.setDateEnd(dateFormat.parse(dateEnd));
        project.setStatus(status);
        projectService.update(project);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/projects/add", method = RequestMethod.GET)
    public ModelAndView addPage() {
        ModelAndView modelAndView = new ModelAndView();
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.PLANNED, Status.INPROCESS, Status.READY));
        modelAndView.addObject(statuses);
        modelAndView.addObject("project", new Project());
        modelAndView.setViewName("project/createProject");
        return modelAndView;
    }

    @RequestMapping(value = "/projects/add", method = RequestMethod.POST)
    public String addProject(@ModelAttribute("name") String name, @ModelAttribute("description") String desc,
                             @ModelAttribute("dateSystem") Date dateSystem, @ModelAttribute("dateStart") String dateStart,
                             @ModelAttribute("dateEnd") String dateEnd, @ModelAttribute("status") Status status) throws ParseException {
        Project project = new Project();
        project.setName(name);
        project.setDescription(desc);
        project.setDateSystem(dateSystem);
        project.setDateStart(dateFormat.parse(dateStart));
        project.setDateEnd(dateFormat.parse(dateEnd));
        project.setProjectId(UUID.randomUUID().toString());
        project.setStatus(status);
        projectService.createProject(project);
        return "redirect:/projects";
    }
}
