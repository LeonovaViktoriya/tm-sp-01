package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.repository.ITaskRepository;
import ru.leonova.tm.entity.Task;

import java.text.SimpleDateFormat;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.mm.yyyy");
    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public List<Task> findAll() {
       return taskRepository.findAll();
    }

    @Override
    public void create(@NotNull final Task task) {
        taskRepository.save(task);
    }

    @Override
    public Task findOneById(@NotNull final String id) {
        return taskRepository.getOne(id);
    }

    @Override
    public void removeTask(@NotNull final Task task) {
       taskRepository.delete(task);
    }

    @Override
    public void removeAllTasksByProjectId(@NotNull final String projectId) {
        taskRepository.deleteAllByProject_ProjectId(projectId);
    }

    @Override
    public void save(@NotNull final Task task) {
        taskRepository.save(task);
    }
    @Override
    public List<Task> findAllByProjectId(@NotNull final String id) {
        return taskRepository.findAllByProject_ProjectId(id);
    }

    @Override
    public void update(@NotNull Task task) {
        if (task.getTaskId().isEmpty() || task.getName().isEmpty() || task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new IllegalArgumentException();
        }
        taskRepository.update(task.getTaskId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateEnd(), task.getStatus());
    }
}
