package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    void create(Task task);

    Task findOneById(String id);

    void removeTask(Task task);

    void removeAllTasksByProjectId(@NotNull String projectId);

    void save(Task task);

    List<Task> findAllByProjectId(@NotNull String id);

    @Transactional
    void update(@NotNull Task task);
}
