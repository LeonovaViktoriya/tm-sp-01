package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project findOneById(@NotNull String projectId);

    void save(Project project);

    void removeProject(@NotNull Project project);

    void createProject(Project project);

    @Transactional
    void update(@NotNull Project project);
}
