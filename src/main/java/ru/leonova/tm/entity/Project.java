package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.leonova.tm.enumerated.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_project")
public final class Project implements Serializable {

    @Id
    @Column(name = "id")
    private String projectId;
    private String name;
    private String description;
    @Column(name = "createDate")
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateSystem;
    @Column(name = "beginEnd")
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateStart;
    @Column(name = "endDate")
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateEnd;

    @Column(name = "statusType")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "project")
    private List<Task> tasks;

}
