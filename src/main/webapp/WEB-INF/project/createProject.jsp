<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="b" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 06.03.2020
  Time: 13:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style type="text/css">
        div.form-row{
            margin:10px;
            width: 30%;
        }
        input.btn{
            margin:10px;
            width: 5%;
        }
    </style>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>Create project</title>

</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand">Create project</a>
    </nav>
    <c:url value="/projects/add" var="var"/>
    <form:form method="POST" action="/projects/add" modelAttribute="project">
        <div class="form-row">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control">
        </div>
        <div class="form-row">
            <label for="description">Description</label>
            <input type="text" name="description" id="description" class="form-control">
        </div>
        <div class="form-row">
            <label for="dateStart">date Start</label>
            <input type="text" name="dateStart" id="dateStart" class="form-control">
        </div>
        <div class="form-row">
            <label for="dateEnd">date End</label>
            <input type="text" name="dateEnd" id="dateEnd" class="form-control">
        </div>
        <div class="form-row">
            <label for="status">Выберите status</label>
            <form:select path="status" id="status" name="status" class="form-control">
                <c:forEach items="${statusList}" var="status" >
                    <option id="status" name="status" value=${status}>${status}</option>
                </c:forEach>
            </form:select>
        </div>
        <input type="submit" value="Add" class="btn btn-success">
    </form:form>
</body>
</html>
