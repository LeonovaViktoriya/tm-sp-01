<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12.02.2020
  Time: 10:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <style type="text/css">
    div.form-row{
    margin:10px;
        width: 30%;
    }
    input.btn{
        margin:10px;
        width: 5%;
    }
    </style>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>Edit project</title>

</head>
<body>
        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand">Update project</a>
        </nav>
        <form:form method="POST" action="/projects/edit/${project.projectId}" modelAttribute="project">
            <div class="form-row">
                <label for="name">Name</label>
                <form:input path="name" type="text" name="name" id="name" class="form-control"/>
            </div>
            <div class="form-row">
                <label for="description">Description</label>
                <form:input path="description" type="text" name="description" id="description" class="form-control"/>
            </div>
            <div class="form-row">
                <label for="dateStart">date Start</label>
                <form:input path="dateStart" type="text" name="dateStart" id="dateStart" class="form-control"/>
            </div>
            <div class="form-row">
                <label for="dateEnd">date End</label>
                <form:input path="dateEnd" type="text" name="dateEnd" id="dateEnd" class="form-control"/>
            </div>

            <div class="form-row">
                <label for="status">Выберите status</label>
                <form:select path="status" id="status" name="status" class="form-control">
                    <c:forEach items="${statusList}" var="status" >
                        <option id="status" name="status" value=${status}>${status}</option>
                    </c:forEach>
                </form:select>
            </div>
            <input type="submit" value="Edit" class="btn btn-success">
        </form:form>

</body>
</html>
