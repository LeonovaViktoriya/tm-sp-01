<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="b" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
<%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/print.css">--%>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <title>Project manager</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="/projects">List projects</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Task-manager <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/tasks">List tasks <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
</nav>
<h2 class="display-3">List all projects</h2>
<br/>
<a class="btn btn-primary"  href="/projects/add" role="button">Create project</a>
<br/>
<br/>
<table class="table table-bordered">
    <thead class="thead-dark">
    <tr>
        <th scope="col" >id</th>
        <th scope="col">name</th>
        <th scope="col">description</th>
        <th scope="col">dateStart</th>
        <th scope="col">dateEnd</th>
        <th scope="col">dateSystem</th>
        <th scope="col">status</th>
        <th scope="col">tasks</th>
        <th scope="col">action</th>
    </tr>
    </thead>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>${project.projectId}</td>
            <td>${project.name}</td>
            <td>${project.description}</td>
            <td><b:formatDate value="${project.dateStart}" pattern="dd.mm.yyyy"/></td>
            <td><b:formatDate value="${project.dateEnd}" pattern="dd.mm.yyyy"/></td>
            <td><b:formatDate value="${project.dateSystem}" pattern="dd.mm.yyyy"/></td>
            <td>${project.status}</td>
            <td> <a href="/tasks/${project.projectId}" type="button" class="btn btn-outline-info">Tasks</a></td>
            <td>
                <a href="/tasks/createForProject/${project.projectId}" type="button" class="btn btn-outline-info">create</a>
                <a href="/projects/edit/${project.projectId}" type="button" class="btn btn-outline-info">edit</a>
                <a href="/projects/delete/${project.projectId}" type="button" class="btn btn-outline-info">delete</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</body>
</html>
