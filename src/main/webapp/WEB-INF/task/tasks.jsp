<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>Tasks</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
    <c:if test="${empty id}">
    <a class="navbar-brand" href="/tasks">List all tasks</a>
    </c:if>
    <c:if test="${!empty id}">
        <a class="navbar-brand" href="/tasks">List tasks project</a>
    </c:if>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Task-manager  <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/projects">List projects <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
</nav>
<c:if test="${empty id}">
    <h2 class="display-3">List all tasks</h2>
</c:if>
<c:if test="${!empty id}">
    <h2 class="display-3">List tasks this project</h2>
</c:if>

<br/>
<%--<a class="btn btn-primary" href="/tasks/add" role="button">Create task</a>--%>
<table class="table table-bordered">
    <thead class="thead-dark">
    <tr>
        <th scope="col">id</th>
        <th scope="col">name</th>
        <th scope="col">description</th>
        <th scope="col">project</th>
        <th scope="col">dateStart</th>
        <th scope="col">dateEnd</th>
        <th scope="col">dateSystem</th>
        <th scope="col">status</th>
        <th scope="col">action</th>
    </tr>
    </thead>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>${task.taskId}</td>
            <td>${task.name}</td>
            <td>${task.description}</td>
            <td>${task.project.projectId}</td>
            <td><fmt:formatDate value="${task.dateStart}" pattern="dd.mm.yyyy"/></td>
            <td><fmt:formatDate value="${task.dateEnd}" pattern="dd.mm.yyyy"/></td>
            <td><fmt:formatDate value="${task.dateSystem}" pattern="dd.mm.yyyy"/></td>
            <td>${task.status}</td>
            <td>
                <a href="/tasks/edit/${task.taskId}" type="button" class="btn btn-outline-info">refresh</a>
                <a href="/tasks/delete/${task.taskId}" type="button" class="btn btn-outline-info">delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
