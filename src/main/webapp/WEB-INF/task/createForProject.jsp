<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 14.02.2020
  Time: 10:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create task</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <style type="text/css">
        div.form-row{
            margin:10px;
            width: 30%;
        }
        input.btn{
            margin:10px;
            width: 5%;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand">Create task</a>
    <a class="btn btn-primary"  href="/" role="button">Home</a>
    <a class="btn btn-primary"  href="/tasks" role="button">Tasks</a>
</nav>
<form:form method="POST" action="/tasks/add/${id}" modelAttribute="task" id="createTask">
    <div class="form-row">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control">
    </div>
    <div class="form-row">
        <label for="description">Description</label>
        <input type="text" name="description" id="description" class="form-control">
    </div>
    <div class="form-row">
        <label for="dateStart">date Start</label>
        <input type="text" name="dateStart" id="dateStart" class="form-control">
    </div>
    <div class="form-row">
        <label for="dateEnd">date End</label>
        <input type="text" name="dateEnd" id="dateEnd" class="form-control">
    </div>

    <div class="form-row">
        <label for="status">Выберите status</label>
        <form:select path="status" id="status" name="status" class="form-control">
            <c:forEach items="${statusList}" var="status" >
                <option id="status" name="status" value=${status}>${status}</option>
            </c:forEach>
        </form:select>
    </div>
    <input type="submit" value="Add" class="btn btn-success">
</form:form>
</body>
</html>
